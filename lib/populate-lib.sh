#! /bin/sh
set -e

# Variables
LIB_PATH=/usr/local/lib

cd `dirname $0`
# Build libasync
make -C ../libasync static

# Boost libraries
cp ${LIB_PATH}/libboost_regex.dylib ./
# Change permission
chmod 755 *.dylib
# Update dependency path
install_name_tool -id '@rpath/libboost_regex.dylib' ./libboost_regex.dylib
