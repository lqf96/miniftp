# Miniftp
Miniftp, a concise and easy-to-use FTP client.

## Features
* Support `USER`, `PASS`, `RETR`, `STOR`, `QUIT`, `TYPE`, `PORT`, `PASV`, `RMD`, `DELE` methods.
* Preserve and show raw FTP logs for professional users, while providing clean and user-friendly user interface at the same time.
* Based on [libasync](libasync), asynchronous, event-driven networking library. The UI is written is Swift, superseding C++, and networking-releated stuff are bridged with Objective-C++ code.

## Usage
* Enter FTP path in the text field and click `Go` to connect to given server and `cd` to given path.
* Click on `Up` will bring you to the parent directory.
* Use `Upload` to upload files to current directory.
* Tick `Passive` option to transfer data in passive mode (default), or untick it for active mode.
* Double click a folder entry will `cd` into the folder. Double click a file will download it.
* Right click on a entry and select `Delete` will try to delete the file or folder. (Currently cannot remove non-empty folder)

## Build
* `cd` into `lib`, and run `populate-lib.sh` to populate libraries used by this project. Make sure Boost is installed before running the script. (Just brew it with `brew install boost`)
* Open this project in Xcode, and build it. You're done.

## Project Structure
* `bridge`
  - `bridge.(cpp|h)`: Bridge a few `libasync` functions to Swift.
  - `data_conn.(cpp|h)`: FTP data connection classes for active mode and passive mode.
  - `session.(mm|h)`: FTP session class.
* `lib`
  - `populate-lib.sh`: Populate dynamic and static libraries needed by the project.
* `miniftp`
  - `Base.lproj`
    + `Main.storyboard`: UI storyboard file.
  - `ViewController.swift`: UI view controller code.

## License
[MIT License](LICENSE)
