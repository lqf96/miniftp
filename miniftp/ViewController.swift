//
//  ViewController.swift
//  miniftp
//
//  Created by 鲁其璠 on 2016/10/12.
//  Copyright © 2016年 Qifan Lu. All rights reserved.
//

import Cocoa
import Dispatch

//File list item type
typealias FileListItem = [Any?]

//Constants
let PROMPT_LINE = "!!! ";

//Optional value compare
func opt_cmp<T: Comparable>(_ a: T?, _ b: T?) -> Bool {
    if let a = a, let b = b {
        return a == b
    } else {
        return a == nil && b == nil
    }
}

//Show message box
func msgbox(content: String, title: String) {
    let alert = NSAlert()
    
    //Set message box
    alert.messageText = title
    alert.informativeText = content
    alert.addButton(withTitle: "OK")
    //Show dialog box
    alert.runModal()
}

//Show prompt box
func prompt(content: String, title: String, default_value: String = "") -> String? {
    let alert = NSAlert()
    let input = NSTextField(frame: NSMakeRect(0, 0, 250, 24))
    
    //Set message box
    alert.messageText = title
    alert.informativeText = content
    alert.addButton(withTitle: "OK")
    alert.addButton(withTitle: "Cancel")
    //Add text field as accessory view
    input.stringValue = default_value
    alert.accessoryView = input
    
    //Show dialog box
    let select_btn = alert.runModal()
    //OK
    if (select_btn==NSAlertFirstButtonReturn) {
        return input.stringValue
    } else {
        return nil
    }
}

//Run taskloop once wrapper
func run_once_wrapper() {
    run_task_loop_once()
    //Run on next tick
    DispatchQueue.main.async(execute: run_once_wrapper)
}

//View controller
class ViewController: NSViewController, NSTableViewDataSource, NSOutlineViewDataSource {
    //FTP session
    var session: FTPSession? = nil
    //Current URL
    var url = URL(string: "miniftp://blank/")!
    //Passive bode
    var passive_mode = true
    
    //Pop-up menu
    let pop_up_menu = NSMenu()
    //Download menu item
    let download_item = NSMenuItem(
        title: "Download",
        action: #selector(ViewController.download(_:)),
        keyEquivalent: ""
    )
    //Delete menu item
    let delete_item = NSMenuItem(
        title: "Delete",
        action: #selector(ViewController.delete(_:)),
        keyEquivalent: ""
    )
    
    //Address bar
    @IBOutlet weak var addr_bar: NSTextField!
    //Log text area
    @IBOutlet weak var log_text: NSTextView!
    //File list view
    @IBOutlet weak var file_list_view: NSTableView!
    
    //File list data
    var file_list = [FileListItem]()
    
    //View loaded
    override func viewDidLoad() {
        //Call super class method
        super.viewDidLoad()
        
        //Initialize libasync modules
        promise_init()
        reactor_init()
        //Insert libasync task loop into GCD's main loop
        run_once_wrapper()
        
        //Add to pop-up menu
        self.pop_up_menu.addItem(self.download_item)
        self.pop_up_menu.addItem(self.delete_item)
        //Set as file list view's contextual menu
        self.file_list_view.menu = self.pop_up_menu
        
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    //Print log
    func print_log(lines: [String]) {
        var str = self.log_text.string ?? ""
        str += lines.joined()
        self.log_text.string = str
    }
    
    //Change working directory
    func cd(dest: String) {
        //Set current working directory
        self.session?.cd(dest) { (success, lines, _) in
            //Print log
            self.print_log(lines: lines! as! [String])
            //Failed
            if !success {
                msgbox(content: "Failed to change working directory.", title: "Error")
                return
            }
            
            //Update working directory
            self.update_cwd()
        }
    }
    
    //Update working directory
    func update_cwd() {
        //Get current working directory
        self.session?.getcwd { (success, lines, result) in
            //Print log
            self.print_log(lines: lines! as! [String])
            //Failed
            if !success {
                msgbox(content: "Failed to get current working directory.", title: "Error")
                return
            }
            
            //Escape new path
            var new_path = String(data: result!, encoding: .utf8) ??
                String(data: result!, encoding: .ascii)!
            new_path = new_path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            //Update URL and address bar
            self.url = URL(string: new_path, relativeTo: self.url)!
            self.addr_bar.stringValue = self.url.absoluteString.removingPercentEncoding!
            
            //List current directory
            self.list_cwd()
        }
    }
    
    //List current directory
    func list_cwd() {
        //List current working directory
        self.session?.list(nil) { (success, lines, result) in
            //Print log
            self.print_log(lines: lines! as! [String])
            //Failed
            if !success {
                msgbox(content: "Failed to list current working directory.", title: "Error")
                return
            }
            
            //Clear file list
            self.file_list = []
            
            //Parse "ls" result and add to file list
            let list_result = String(data: result!, encoding: .utf8) ??
                String(data: result!, encoding: .ascii)!
            for line in list_result.components(separatedBy: "\n") {
                if line.isEmpty {
                    continue
                }
                //Parse each line
                let items = line.components(separatedBy: CharacterSet(charactersIn: " \t"))
                    .filter { (str) in !str.isEmpty }
                
                //File type & size
                var file_type = "", file_size: UInt? = nil
                switch items[0].characters.first! {
                case "-":
                    file_type = "File"
                    file_size = UInt(items[4])
                case "d":
                    file_type = "Directory"
                default:
                    continue
                }
                //Last modified date
                var date_builder = NSCalendar.current.dateComponents([.year], from: Date())
                date_builder.calendar = NSCalendar.current
                date_builder.month = Int(items[5])
                date_builder.day = Int(items[6])
                if let year = Int(items[7]) {
                    //Year provided
                    date_builder.year = year
                } else {
                    let time_items = items[7].components(separatedBy: ":")
                    //Set hour and minute
                    date_builder.hour = Int(time_items[0])
                    date_builder.minute = Int(time_items[1])
                }
                //File name
                let file_name = items[8..<items.count].joined(separator: " ")
                
                //Add to file list
                self.file_list.append([
                    file_name,
                    date_builder.date,
                    file_size,
                    file_type
                ])
            }
            
            //Update file list view
            self.file_list_view.reloadData()
        }
    }
    
    //Go to destination address
    @IBAction func go(_ sender: NSButton) {
        //Parse address bar URL
        let url_str = self.addr_bar.stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        guard var new_url = URL(string: url_str) else {
            msgbox(content: "Invalid FTP URL.", title: "Error")
            return
        }
        //Invalid URL
        if new_url.scheme != "ftp" || new_url.host == nil {
            msgbox(content: "Invalid FTP URL.", title: "Error")
            return
        }
        
        //Reconnect flag
        let reconnect_flag = !opt_cmp(new_url.user, self.url.user)
            || !opt_cmp(new_url.host, self.url.host)
            || !opt_cmp(new_url.port, self.url.port)
        //Change directory flag
        let cd_flag = !reconnect_flag && !opt_cmp(new_url.path, self.url.path)
        
        if reconnect_flag {
            //Replace old session with a new one
            self.session = FTPSession()
            //Set data transfer mode
            self.session?.passive = self.passive_mode
            
            //Try to connect to remote server
            let port = new_url.port ?? 21
            self.session?.connect(new_url.host!, port: UInt16(port)) { (success, lines, _) in
                //Print log
                self.print_log(lines: [PROMPT_LINE+"Connected to new server.\n"])
                self.print_log(lines: lines! as! [String])
                
                //Failed to connect
                if !success {
                    msgbox(content: "Failed to connect to remote server.", title: "Error")
                    //Clear FTP session
                    self.session = nil
                    
                    return
                }
                
                //Set URL path
                if new_url.path.isEmpty {
                    new_url = URL(string: "/", relativeTo: new_url)!
                }
                
                //Ask for username
                guard let username = new_url.user ?? prompt(content: "Enter username:", title: "Username required") else {
                    msgbox(content: "No username given.", title: "Error")
                    //Clear FTP session
                    self.session = nil
                    
                    return
                }
                //Ask for password
                guard let password = prompt(content: "Enter FTP password:", title: "Password required") else {
                    msgbox(content: "No password given.", title: "Error")
                    //Clear FTP session
                    self.session = nil
                    
                    return
                }
                
                //Try to log-in
                self.session?.login(username, password: password) { (success, lines, _) in
                    //Print log
                    self.print_log(lines: lines! as! [String])
                    
                    //Failed to log-in
                    if !success {
                        msgbox(content: "Failed to log-in.", title: "Error")
                        //Clear FTP session
                        self.session = nil
                        
                        return
                    }
                    
                    //Set new URL
                    self.url = new_url
                    //Change working directory
                    self.cd(dest: new_url.path.removingPercentEncoding!)
                }
            }
        } else if cd_flag {
            //Change working directory
            self.cd(dest: new_url.path)
        } else {
            //List current directory
            self.list_cwd()
        }
    }
    
    //Change to parent directory
    @IBAction func up(_ sender: NSButton) {
        //Go up
        self.session?.cdup { (success, lines, _) in
            //Print log
            self.print_log(lines: lines! as! [String])
            //Failed
            if !success {
                msgbox(content: "Failed to change to parent directory.", title: "Error")
                return
            }
            
            //Update current working directory
            self.update_cwd()
        }
    }
    
    //Upload file
    @IBAction func upload(_ sender: NSButton) {
        //Open file dialog
        let file_dialog = NSOpenPanel()
        file_dialog.runModal()
        //Get path of the file chosen
        guard let file_url = file_dialog.url else {
            return
        }
        
        //Read file content
        do {
            let upload_data = try Data(contentsOf: file_url)
            //Send upload data to server
            self.session?.put(file_url.lastPathComponent, data: upload_data) { (success, lines, _) in
                //Print log
                self.print_log(lines: lines! as! [String])
                
                //Failed
                if !success {
                    msgbox(content: "Failed to upload file.", title: "Error")
                    return
                }
                
                //Refresh current directory file list
                self.list_cwd()
            }
        } catch {
            msgbox(content: "Failed to read file content for upload.", title: "Error")
            return
        }
    }
    
    //Get number of rows in table view
    func numberOfRows(in file_list_view: NSTableView) -> Int {
        return self.file_list.count
    }
    
    //Object value for table column
    func tableView(_ file_list_view: NSTableView, objectValueFor column: NSTableColumn?, row: Int) -> Any? {
        let index_str = column?.identifier.characters.last!.description
        return self.file_list[row][Int(index_str!)!]
    }
    
    //Download given file
    func download(_ sender: Any?) {
        //Do nothing with no active session or clicked row
        if self.session == nil || self.file_list_view.clickedRow < 0 {
            return
        }
        let row_index = self.file_list_view.clickedRow
        
        //File name and type
        let data_row = self.file_list[row_index],
        file_name = data_row[0] as! String,
        file_type = data_row[3] as! String
        //Can only download file
        if file_type != "File" {
            return
        }
        
        //Save file dialog
        let file_dialog = NSSavePanel()
        file_dialog.runModal()
        //Get path of the file to be saved
        guard let file_url = file_dialog.url else {
            return
        }
        
        //Fetch file from remote
        self.session?.get(file_name) { (success, lines, content) in
            //Print log
            self.print_log(lines: lines! as! [String])
            
            //Failed
            if !success {
                msgbox(content: "Failed to download given file.", title: "Error")
                return
            }
            
            //Save file content to given URL
            do {
                try content!.write(to: file_url)
            } catch {
                msgbox(content: "Failed to write to given path.", title: "Error")
                return
            }
            
            //Successfully saved file
            msgbox(content: "File \"\(file_name)\" successfully downloaded to \"\(file_url.path)\".", title: "Download Completed")
        }
    }
    
    //Delete given file or folder
    func delete(_ sender: Any?) {
        //Do nothing with no active session or clicked row
        if self.session == nil || self.file_list_view.clickedRow < 0 {
            return
        }
        let row_index = self.file_list_view.clickedRow
        
        //Remove file or folder name
        let data_row = self.file_list[row_index],
            file_name = data_row[0] as! String,
            file_type = data_row[3] as! String
        //Operation callback
        let oper_callback = { (success: Bool, lines: [Any]?, _: Data?) in
            //Print log
            self.print_log(lines: lines! as! [String])
            
            //Failed
            if !success {
                msgbox(content: "Failed to remove given file or folder.", title: "Error")
                return
            }
            
            //List current directory again
            self.list_cwd()
        }
        
        //File type
        switch file_type {
        case "File":
            self.session?.rm(file_name, callback: oper_callback)
        case "Directory":
            self.session?.rmdir(file_name, callback: oper_callback)
        default:
            break
        }
    }
    
    //Change data transfer mode
    @IBAction func change_mode(_ sender: NSButton) {
        //Passive mode flag
        self.passive_mode = sender.state == NSOnState
        self.session?.passive = self.passive_mode
    }
    
    //Double click to open file
    @IBAction func open_file(_ sender: NSTableView) {
        //Do nothing with no active session or clicked row
        if self.session == nil || self.file_list_view.clickedRow < 0 {
            return
        }
        let row_index = self.file_list_view.clickedRow
        
        //Row, file name and file type
        let row = self.file_list[row_index],
            file_name = row[0]! as! String,
            file_type = row[3]! as! String
        //File type
        switch file_type {
        case "File":
            self.download(nil)
        case "Directory":
            self.cd(dest: file_name)
        default:
            break
        }
    }
}

