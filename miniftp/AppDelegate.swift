//
//  AppDelegate.swift
//  miniftp
//
//  Created by 鲁其璠 on 2016/10/12.
//  Copyright © 2016年 Qifan Lu. All rights reserved.
//

import Cocoa
import Dispatch

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    //Application initialization
    func applicationDidFinishLaunching(_ aNotification: Notification) { }
    //Application termination
    func applicationWillTerminate(_ aNotification: Notification) { }
}
