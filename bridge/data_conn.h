#pragma once

#include <netinet/in.h>
#include <string>
#include <boost/optional.hpp>
#include <libasync/socket.h>
#include <libasync/promise.h>

using std::string;
using boost::optional;
using libasync::Promise;
using libasync::Socket;
using libasync::ServerSocket;

//Data connection base class
class DataConnection {
public:
    //Send data
    virtual Promise<void> send(string str) = 0;
    //Receive data
    virtual Promise<string> recv() = 0;
    
    //Virtual destructor
    virtual ~DataConnection() {}
};

//Active connection class
class ActiveConnection : public DataConnection {
    //Server socket
    ServerSocket server_sock;
    //Socket
    optional<Socket> sock;
    
    //Received data
    string recv_data;
public:
    //Constructor
    ActiveConnection();
    //Destructor
    ~ActiveConnection();
    
    //Send data
    Promise<void> send(string str);
    //Receive data
    Promise<string> recv();
    
    //Get port
    in_port_t port();
};

//Passive connection class
class PassiveConnection : public DataConnection {
    //Socket
    Socket sock;
    
    //Server address
    in_addr_t addr;
    //Server port
    in_addr_t port;
    
    //Received data
    string recv_data;
public:
    //Constructor
    PassiveConnection(in_addr_t _addr, in_port_t _port);
    //Destructor
    ~PassiveConnection();
    
    //Send data
    Promise<void> send(string str);
    //Receive data
    Promise<string> recv();
};
