#include <libasync/promise.h>
#include <libasync/reactor.h>
#include <libasync/taskloop.h>
#include "bridge.h"

using libasync::TaskLoop;

extern "C" {
    //Initialize promise module (Bridge method)
    void promise_init() {
        libasync::promise_init();
    }

    //Initialize reactor module (Bridge method)
    void reactor_init() {
        libasync::reactor_init();
    }

    //Run task loop once
    void run_task_loop_once() {
        TaskLoop::thread_loop().run_once();
    }
}
