//Session.h: FTP client session
#pragma once

#import <Foundation/Foundation.h>

//Callback types
typedef void (^Callback)(bool, NSArray*, NSData*);

//FTP session class
@interface FTPSession : NSObject
//Passive mode
@property bool passive;

//Constructor
-(instancetype) init;
//Destructor
-(void) dealloc;

//Connect to FTP server
-(void) connect: (NSString*) addr port: (uint16_t) port callback: (Callback) callback;
//Log-in to FTP server
-(void) login: (NSString*) username password: (NSString*) password callback: (Callback) callback;

//Get file
-(void) get: (NSString*) name callback: (Callback) callback;
//Put file
-(void) put: (NSString*) name data: (NSData*) data callback: (Callback) callback;
//List path
-(void) list: (NSString*) name callback: (Callback) callback;

//Get current working directory
-(void) getcwd: (Callback) callback;
//Change directory
-(void) cd: (NSString*) pos callback: (Callback) callback;
//Go one level up
-(void) cdup: (Callback) callback;

//Make directory
-(void) mkdir: (NSString*) name callback: (Callback) callback;
//Remove file
-(void) rm: (NSString*) name callback: (Callback) callback;
//Remove directory
-(void) rmdir: (NSString*) name callback: (Callback) callback;
//Move file
-(void) mv: (NSString*) from to: (NSString*) to callback: (Callback) callback;
@end
