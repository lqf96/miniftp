#include <arpa/inet.h>
#include <netdb.h>
#include <string>
#include <utility>
#include <sstream>
#include <list>
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/algorithm/string/join.hpp>
#include <libasync/socket.h>
#include <libasync/promise.h>
#include <libasync/taskloop.h>
#include "session.h"
#include "bridge.h"
#include "data_conn.h"

using std::string;
using std::pair;
using std::make_pair;
using std::ostringstream;
using std::list;
using std::stoi;
using boost::regex;
using boost::smatch;
using boost::regex_search;
using boost::algorithm::split_regex;
using boost::algorithm::join;
using libasync::Socket;
using libasync::Promise;
using libasync::PromiseCtx;
using libasync::SocketError;

//Intermidiate callback type
typedef void (^IntCallback)(bool, NSMutableArray*);

//Sent and received line
const char SENT_LINE[] = "<<< ";
const char RECV_LINE[] = ">>> ";
const char ERROR_LINE[] = "!!! ";
//Rejected flag
const uint16_t LOGIN_REJECTED = 0;

//Convert C++ string to NSString
NSString* objc_str_from_cxx(string str) {
    return [NSString stringWithUTF8String: str.c_str()];
}

//Convert C++ string to NSData
NSData* objc_data_from_cxx(string str) {
    return [NSData dataWithBytes: str.c_str() length: str.length()];
}

//Convert NSString to C++ string
string cxx_str_from_objc(NSString* str) {
    return string([str UTF8String]);
}

//Convert NSData to C++ string
string cxx_str_from_objc(NSData* data) {
    return string((const char*)[data bytes], (size_t)data.length);
}

//Make request
string make_req(string method, string arg = "") {
    string result = method;
    
    if (!arg.empty())
        result += ' '+arg;
    result += "\r\n";
    
    return result;
}

//Parse response
pair<unsigned int, string> parse_resp(string resp, string* remaining = nullptr) {
    list<string> split_resp;
    regex split_rx("\r\n");
    regex line_rx("(\\d+)([- ])(.*)");
    pair<unsigned int, string> result;
    
    //Split response by "\r\n"
    split_regex(split_resp, resp, split_rx);
    //Parse each line
    while (split_resp.size()>0) {
        string line = split_resp.front();
        split_resp.pop_front();
        //Match line regular expression
        smatch rx_match;
        bool match = regex_search(line, rx_match, line_rx);
        
        //Illegal response
        if (!match)
            return make_pair(0, "");
        //Append to text
        if (!result.second.empty())
            result.second += '\n';
        result.second += rx_match[3];
        
        //End of response
        if (rx_match[2]==" ") {
            result.first = stoi(rx_match[1]);
            break;
        }
    }
    //Return remaining parts
    if (remaining)
        *remaining = join(split_resp, "\r\n");
    
    return result;
}

//FTP session class
@interface FTPSession()
//Socket
@property Socket sock;
//On data callback
@property void (^on_data)(string);
//Data connection
@property DataConnection* conn;

//Request active connection
-(void) request_active_conn: (IntCallback) callback;
//Request passive connection
-(void) request_passive_conn: (IntCallback) callback;
@end

@implementation FTPSession
//Constructor
-(instancetype) init {
    self = [super init];
    
    //Base class initialized
    if (self) {
        //Data connection
        self.conn = nullptr;
        //Passive mode
        self.passive = true;
        
        //Listen on data event
        self.sock.on("data", [=](string data) mutable {
            self.on_data(data);
        });
    }
    
    return self;
}

//Destructor
-(void) dealloc {
    //Data connection
    if (self.conn)
        delete self.conn;
    //Close socket
    self.sock.close();
}

//Connect to FTP server
-(void) connect: (NSString*) addr port: (uint16_t) port callback: (Callback) callback {
    in_addr _addr;
    
    //Convert to address type
    int convert_status = inet_aton([addr UTF8String], &_addr);
    //Failed; try to resolve hostname
    if (!convert_status) {
        auto resolve_result = gethostbyname([addr UTF8String]);
        //Failed to resolve host name
        if (!resolve_result) {
            string error_line;
            error_line += ERROR_LINE;
            error_line += "Failed to resolve host name.\n";
            //Invoke callback
            callback(false, [NSArray arrayWithObjects: objc_str_from_cxx(error_line), nil], nil);
        }
        
        //Save resolve result
        auto addr_list = (in_addr**)resolve_result->h_addr_list;
        _addr = *(addr_list[0]);
    }
    
    //Set data callback
    self.on_data = ^(string resp) {
        callback(true, [NSArray arrayWithObjects: objc_str_from_cxx(RECV_LINE+resp), nil], nil);
    };
    //Handle connection refused error
    self.sock.on("error", [=](SocketError e) mutable {
        //Make error line
        string error_line;
        error_line += ERROR_LINE;
        error_line += e.what();
        error_line += '\n';
        //Invoke callback
        callback(false, [NSArray arrayWithObjects: objc_str_from_cxx(error_line), nil], nil);
    });
    
    //Connect to server
    self.sock.connect(_addr.s_addr, htons(port));
}

//Log-in to FTP server
-(void) login: (NSString*) username password: (NSString*) password callback: (Callback) callback {
    //Wait for data
    auto wait_for_data = [=]() mutable {
        return Promise<string>([=](PromiseCtx<string> _ctx) mutable {
            __block PromiseCtx<string> ctx = _ctx;
            self.on_data = ^(string data) {
                ctx.resolve(data);
            };
        });
    };
    
    //Make request
    auto req = make_req("USER", cxx_str_from_objc(username));
    //Lines
    NSMutableArray* lines = [NSMutableArray arrayWithObjects:
                             objc_str_from_cxx(SENT_LINE+req),
                             nil];
    
    //Send username request
    self.sock.write(req).then<string>(wait_for_data)
        .then<void>([=](string data) mutable {
            //Append to lines
            [lines addObject: objc_str_from_cxx(RECV_LINE+data)];
            //Parse response
            auto resp_pair = parse_resp(data);
            
            //Failed
            if ((resp_pair.first!=230)&&(resp_pair.first!=331)&&(resp_pair.first!=332)) {
                callback(false, lines, nil);
                throw LOGIN_REJECTED;
            }
            
            //Make request
            auto req = make_req("PASS", cxx_str_from_objc(password));
            //Append to lines
            [lines addObject: objc_str_from_cxx(SENT_LINE+req)];
            
            //Send password request
            return self.sock.write(req);
        }).then<string>(wait_for_data)
        .then<void>([=](string data) mutable {
            //Append to lines
            [lines addObject: objc_str_from_cxx(RECV_LINE+data)];
            //Parse response
            auto resp_pair = parse_resp(data);
            
            //Failed
            if ((resp_pair.first!=202)&&(resp_pair.first!=230)) {
                callback(false, lines, nil);
                throw LOGIN_REJECTED;
            }
            
            //Make request
            auto req = make_req("TYPE", "I");
            //Append to lines
            [lines addObject: objc_str_from_cxx(SENT_LINE+req)];
            
            //Send type request
            return self.sock.write(req);
        }).then<string>(wait_for_data)
        .then<void>([=](string data) mutable {
            //Append to lines
            [lines addObject: objc_str_from_cxx(RECV_LINE+data)];
            //Parse response
            auto resp_pair = parse_resp(data);
            
            //Success
            bool success = resp_pair.first==200;
            //Invoke callback
            callback(success, lines, nil);
        });
}

//Get file
-(void) get: (NSString*) name callback: (Callback) callback {
    __weak FTPSession* _self = self;
    //Connection creation callback
    IntCallback conn_created = ^(bool success, NSMutableArray* lines) {
        //Failed
        if (!success) {
            callback(false, lines, nil);
            return;
        }
        
        //Make request
        auto req = make_req("RETR", cxx_str_from_objc(name));
        //Append to lines
        [lines addObject: objc_str_from_cxx(SENT_LINE+req)];
        
        //Send request
        self.sock.write(req);
        //Receive data
        __block auto recv_data_target = self.conn->recv();
        
        //Connection established message received
        _self.on_data = ^(string data) {
            //Append to lines
            [lines addObject: objc_str_from_cxx(RECV_LINE+data)];
            //Parse response
            auto resp_pair = parse_resp(data, &data);
            
            //Failed
            if (resp_pair.first!=150) {
                //Remove connection
                delete _self.conn;
                _self.conn = nullptr;
                //Failed callback
                callback(false, lines, nil);
                return;
            }
            
            //Download completed
            void (^download_completed)(string) = ^(string data2) {
                //Append to lines
                if (data.empty())
                    [lines addObject: objc_str_from_cxx(RECV_LINE+data2)];
                //Parse response
                auto resp_pair = parse_resp(data2);
                
                //Success
                if (resp_pair.first==226)
                    recv_data_target.then<void>([=](string recv_data) {
                        callback(true, lines, objc_data_from_cxx(recv_data));
                    });
                //Failed
                else
                    callback(false, lines, nil);
            };
            
            //Download already completed
            if (!data.empty())
                download_completed(data);
            //Not completed
            else
                self.on_data = download_completed;
        };
    };
    
    //Request new connection
    if (self.passive)
        [self request_passive_conn: conn_created];
    else
        [self request_active_conn: conn_created];
}

//Put file
-(void) put: (NSString*) name data: (NSData*) data callback: (Callback) callback {
    __weak FTPSession* _self = self;
    //Connection creation callback
    IntCallback conn_created = ^(bool success, NSMutableArray* lines) {
        //Failed
        if (!success) {
            callback(false, lines, nil);
            return;
        }
        
        //Make request
        auto req = make_req("STOR", cxx_str_from_objc(name));
        //Append to lines
        [lines addObject: objc_str_from_cxx(SENT_LINE+req)];
        
        //Connection established message received
        _self.on_data = ^(string data) {
            //Append to lines
            [lines addObject: objc_str_from_cxx(RECV_LINE+data)];
            //Parse response
            auto resp_pair = parse_resp(data, &data);
            
            //Failed; close data connection and return
            if (resp_pair.first!=150) {
                //Remove connection
                delete _self.conn;
                _self.conn = nullptr;
                //Failed callback
                callback(false, lines, nil);
                return;
            }
            
            //Upload complete handler
            void (^upload_completed)(string) = ^(string data2) {
                //Append to lines
                if (data.empty())
                    [lines addObject: objc_str_from_cxx(RECV_LINE+data2)];
                //Parse response
                auto resp_pair = parse_resp(data2);
                
                //Success
                bool success = resp_pair.first==226;
                //Invoke callback
                return callback(success, lines, nil);
            };
            
            //Upload already completed
            if (!data.empty())
                upload_completed(data);
            //Not completed
            else
                self.on_data = upload_completed;
        };
        
        //Send request
        _self.sock.write(req);
        //Send data
        _self.conn->send(cxx_str_from_objc(data));
    };
    
    //Request new connection
    if (self.passive)
        [self request_passive_conn: conn_created];
    else
        [self request_active_conn: conn_created];
}

//List path
-(void) list: (NSString*) name callback: (Callback) callback {
    __weak FTPSession* _self = self;
    //Connection creation callback
    IntCallback conn_created = ^(bool success, NSMutableArray* lines) {
        //Failed
        if (!success) {
            callback(false, lines, nil);
            return;
        }
        
        //Make request
        string list_path = name?cxx_str_from_objc(name):"";
        auto req = make_req("LIST", list_path);
        //Append to lines
        [lines addObject: objc_str_from_cxx(SENT_LINE+req)];
        
        //Send request
        self.sock.write(req);
        //Receive data
        __block auto recv_data_target = self.conn->recv();
        
        //Connection established message received
        _self.on_data = ^(string data) {
            //Append to lines
            [lines addObject: objc_str_from_cxx(RECV_LINE+data)];
            //Parse response
            auto resp_pair = parse_resp(data, &data);
            
            //Failed
            if (resp_pair.first!=150) {
                //Remove connection
                delete _self.conn;
                _self.conn = nullptr;
                //Failed callback
                callback(false, lines, nil);
                return;
            }
            
            //Download complete handler
            void (^download_completed)(string) = ^(string data2) {
                //Append to lines
                if (data.empty())
                    [lines addObject: objc_str_from_cxx(RECV_LINE+data2)];
                //Parse response
                auto resp_pair = parse_resp(data2);
                
                //Success
                if (resp_pair.first==226)
                    recv_data_target.then<void>([=](string recv_data) {
                        callback(true, lines, objc_data_from_cxx(recv_data));
                    });
                //Failed
                else
                    callback(false, lines, nil);
            };
            
            //Download already completed
            if (!data.empty())
                download_completed(data);
            //Not completed
            else
                self.on_data = download_completed;
        };
    };
    
    //Request new connection
    if (self.passive)
        [self request_passive_conn: conn_created];
    else
        [self request_active_conn: conn_created];
}

//Get current working directory
-(void) getcwd: (Callback) callback {
    string req = make_req("PWD");
    
    //Response received
    self.on_data = ^(string data) {
        //Lines
        NSArray* lines = [NSArray arrayWithObjects:
                          objc_str_from_cxx(SENT_LINE+req),
                          objc_str_from_cxx(RECV_LINE+data),
                          nil];
        //Parse response
        auto resp_pair = parse_resp(data);
        //Regular expression
        regex path_rx("\"(.*)\"");
        smatch rx_match;
        
        bool path_extracted = regex_search(resp_pair.second, rx_match, path_rx);
        //Failed
        if ((resp_pair.first!=257)||(!path_extracted))
            callback(false, lines, nil);
        //Success
        else
            callback(true, lines, objc_data_from_cxx(rx_match[1]));
    };
    
    //Send request
    self.sock.write(req);
}

//Change directory
-(void) cd: (NSString*) pos callback: (Callback) callback {
    string _pos = cxx_str_from_objc(pos);
    string req = make_req("CWD", _pos);
    
    //Response received
    self.on_data = ^(string data) {
        //Lines
        NSArray* lines = [NSArray arrayWithObjects:
                          objc_str_from_cxx(SENT_LINE+req),
                          objc_str_from_cxx(RECV_LINE+data),
                          nil];
        //Parse response
        auto resp_pair = parse_resp(data);
        
        //Success
        bool success = (resp_pair.first==200)||(resp_pair.first==250);
        //Invoke callback
        callback(success, lines, nil);
    };
    
    //Send request
    self.sock.write(req);
}

//Go one level up
-(void) cdup: (Callback) callback {
    string req = make_req("CDUP");
    
    //Response received
    self.on_data = ^(string data) {
        //Lines
        NSArray* lines = [NSArray arrayWithObjects:
                          objc_str_from_cxx(SENT_LINE+req),
                          objc_str_from_cxx(RECV_LINE+data),
                          nil];
        //Parse response
        auto resp_pair = parse_resp(data);
        
        //Success
        bool success = (resp_pair.first==200)||(resp_pair.first==250);
        //Invoke callback
        callback(success, lines, nil);
    };
    
    //Send request
    self.sock.write(req);
}

//Make directory
-(void) mkdir: (NSString*) name callback: (Callback) callback {
    string req = make_req("MKD", cxx_str_from_objc(name));
    
    //Response received
    self.on_data = ^(string data) {
        //Lines
        NSArray* lines = [NSArray arrayWithObjects:
                          objc_str_from_cxx(SENT_LINE+req),
                          objc_str_from_cxx(RECV_LINE+data),
                          nil];
        //Parse response
        auto resp_pair = parse_resp(data);
        
        //Success
        bool success = resp_pair.first==250;
        //Invoke callback
        callback(success, lines, nil);
    };
    
    //Send request
    self.sock.write(req);
}

//Remove file
-(void) rm: (NSString*) name callback: (Callback) callback {
    string req = make_req("DELE", cxx_str_from_objc(name));
    
    //Response received
    self.on_data = ^(string data) {
        //Lines
        NSArray* lines = [NSArray arrayWithObjects:
                          objc_str_from_cxx(SENT_LINE+req),
                          objc_str_from_cxx(RECV_LINE+data),
                          nil];
        //Parse response
        auto resp_pair = parse_resp(data);
        
        //Success
        bool success = resp_pair.first==250;
        //Invoke callback
        callback(success, lines, nil);
    };
    
    //Send request
    self.sock.write(req);
}

//Remove directory
-(void) rmdir: (NSString*) name callback: (Callback) callback {
    string req = make_req("RMD", cxx_str_from_objc(name));
    
    //Response received
    self.on_data = ^(string data) {
        //Lines
        NSArray* lines = [NSArray arrayWithObjects:
                          objc_str_from_cxx(SENT_LINE+req),
                          objc_str_from_cxx(RECV_LINE+data),
                          nil];
        //Parse response
        auto resp_pair = parse_resp(data);
        
        //Success
        bool success = resp_pair.first==250;
        //Invoke callback
        callback(success, lines, nil);
    };
    
    //Send request
    self.sock.write(req);
}

//Move file
-(void) mv: (NSString*) from to: (NSString*) to callback: (Callback) callback {
    string rnfr_req = make_req("RNFR", cxx_str_from_objc(from));
    //Lines
    NSMutableArray* lines = [NSMutableArray arrayWithObjects: objc_str_from_cxx(SENT_LINE+rnfr_req), nil];
    
    __weak FTPSession* _self = self;
    //"RNFR" received
    self.on_data = ^(string rnfr_data) {
        //Append to lines
        [lines addObject: objc_str_from_cxx(RECV_LINE+rnfr_data)];
        //Parse response
        auto resp_pair = parse_resp(rnfr_data);
        
        //Failed
        if (resp_pair.first!=350) {
            callback(false, lines, nil);
            return;
        }
        
        //"RNTO" received
        _self.on_data = ^(string rnto_data) {
            //Append to lines
            [lines addObject: objc_str_from_cxx(RECV_LINE+rnto_data)];
            //Parse response
            auto resp_pair = parse_resp(rnto_data);
            
            //Success
            bool success = resp_pair.first==250;
            //Invoke callback
            callback(success, lines, nil);
        };
        
        string rnto_req = make_req("RNTO", cxx_str_from_objc(to));
        //Append to lines
        [lines addObject: objc_str_from_cxx(SENT_LINE+rnto_req)];
        //Send request
        _self.sock.write(rnto_req);
    };
    
    //Send request
    self.sock.write(rnfr_req);
}

//Request active connection
-(void) request_active_conn: (IntCallback) callback {
    in_addr_t addr;
    in_port_t port;
    
    //Get socket address
    self.sock.local_addr(&addr, &port);
    //Create active connection
    if (self.conn)
        delete self.conn;
    ActiveConnection* conn = new ActiveConnection();
    self.conn = conn;
    //Get port
    port = ntohs(conn->port());
    
    ostringstream ss;
    uint8_t* _addr = (uint8_t*)(&addr);
    //Extract IP and port
    uint16_t addr0 = _addr[0],
        addr1 = _addr[1],
        addr2 = _addr[2],
        addr3 = _addr[3];
    uint16_t port0 = port/256,
        port1 = port%256;
    //Write to string stream
    ss<<addr0<<','<<addr1<<','<<addr2<<','<<addr3<<','<<port0<<','<<port1;
    
    //Make request
    string req = make_req("PORT", ss.str());
    
    //Response received
    self.on_data = ^(string data) {
        //Lines
        NSMutableArray* lines = [NSMutableArray arrayWithObjects:
                          objc_str_from_cxx(SENT_LINE+req),
                          objc_str_from_cxx(RECV_LINE+data),
                          nil];
        //Parse response
        auto resp_pair = parse_resp(data);
        
        //Success
        bool success = resp_pair.first==200;
        //Invoke callback
        callback(success, lines);
    };
    
    //Send request
    self.sock.write(req);
}

//Request passive connection
-(void) request_passive_conn: (IntCallback) callback {
    string req = make_req("PASV");
    
    __weak FTPSession* _self = self;
    //Response received
    self.on_data = ^(string data) {
        //Lines
        NSMutableArray* lines = [NSMutableArray arrayWithObjects:
                                 objc_str_from_cxx(SENT_LINE+req),
                                 objc_str_from_cxx(RECV_LINE+data),
                                 nil];
        //Parse response
        auto resp_pair = parse_resp(data);
        //Tuple regular expression
        regex tuple_rx("(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+)");
        smatch rx_match;
        
        //Failed
        if ((resp_pair.first!=227)||(!regex_search(data, rx_match, tuple_rx))) {
            callback(false, lines);
            return;
        }
        
        in_addr_t addr;
        //Convert to IP address
        uint8_t* _addr = (uint8_t*)(&addr);
        _addr[0] = stoi(rx_match[1]);
        _addr[1] = stoi(rx_match[2]);
        _addr[2] = stoi(rx_match[3]);
        _addr[3] = stoi(rx_match[4]);
        //Port
        in_port_t port = stoi(rx_match[5])*256+stoi(rx_match[6]);
        
        //Create passive connection object
        if (_self.conn)
            delete _self.conn;
        _self.conn = new PassiveConnection(addr, htons(port));
        
        //Invoke callback
        callback(true, lines);
    };
    
    //Send request
    self.sock.write(req);
}
@end
