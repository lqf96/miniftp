#include "data_conn.h"

using libasync::PromiseCtx;
using libasync::SocketError;

//Ephemeral port range
const static in_port_t EPH_PORT_BEGIN = 20000;
const static in_port_t EPH_PORT_END = 60000;

//Active connection constructor
ActiveConnection::ActiveConnection() {
    //Listen for connect event
    this->server_sock.on("connect", [=](Socket sock) mutable {
        this->sock = sock;
    });
    
    //Choose a random port for active connection
    while (true)
    {   in_port_t port = rand()%(EPH_PORT_END-EPH_PORT_BEGIN)+EPH_PORT_BEGIN;
        try
        {   this->server_sock.listen(htonl(INADDR_ANY), htons(port));
            break;
        }
        catch (SocketError e)
        {   //Retry if address in use; throw again for other error
            if ((e.reason()!=SocketError::Reason::BIND)||(e.error_num()!=EADDRINUSE))
                throw e;
        }
    }
}

//Active connection destructor
ActiveConnection::~ActiveConnection() {
    if (this->sock)
        this->sock->close();
    this->server_sock.close();
}

//Send data
Promise<void> ActiveConnection::send(string str) {
    //Action
    auto action = [=](Socket sock) mutable {
        //Resolve promise when transfer completed
        return Promise<void>([=](PromiseCtx<void> ctx) mutable {
            sock.write(str).then<void>([=]() mutable {
                sock.close();
                //Resolve promise
                ctx.resolve();
            });
        });
    };
    
    //Client socket connected
    if (this->sock)
        return action(*this->sock);
    //Not connected
    else
        return Promise<void>([=](PromiseCtx<void> ctx) mutable {
            this->server_sock.on("connect", [=](Socket sock) mutable {
                action(sock).then<void>([=]() mutable {
                    ctx.resolve();
                });
            });
        });
}

//Receive data
Promise<string> ActiveConnection::recv() {
    //Action
    auto action = [=](Socket sock) mutable {
        //Data received
        sock.on("data", [=](string data) mutable {
            this->recv_data += data;
        });
        
        //Data transfer completed
        Promise<string> promise([=](PromiseCtx<string> ctx) mutable {
            sock.on("end", [=]() mutable {
                ctx.resolve(this->recv_data);
            });
        });
        
        return promise;
    };
    
    //Client socket connected
    if (this->sock)
        return action(*this->sock);
    //Not connected
    else
        return Promise<string>([=](PromiseCtx<string> ctx) mutable {
            this->server_sock.on("connect", [=](Socket sock) mutable {
                ctx.resolve(action(sock));
            });
        });
}

//Get port
in_port_t ActiveConnection::port() {
    in_addr_t addr;
    in_port_t port;
    
    this->server_sock.local_addr(&addr, &port);
    return port;
}

//Passive connection constructor
PassiveConnection::PassiveConnection(in_addr_t _addr, in_port_t _port)
    : addr(_addr), port(_port) {}

//Passive connection destructor
PassiveConnection::~PassiveConnection() {
    this->sock.close();
}

//Send data
Promise<void> PassiveConnection::send(string str) {
    //Send data promise
    Promise<void> promise([=](PromiseCtx<void> ctx) mutable {
        this->sock.on("connect", [=]() mutable {
            //Write data and close connection
            this->sock.write(str).then<void>([=]() mutable {
                this->sock.close();
                //Resolve promise
                ctx.resolve();
            });
        });
    });
    //Connect to given address and port
    this->sock.connect(this->addr, this->port);
    
    return promise;
}

//Receive data
Promise<string> PassiveConnection::recv() {
    //Append received data
    this->sock.on("data", [=](string data) mutable {
        this->recv_data += data;
    });
    
    //Receive data promise
    Promise<string> promise([=](PromiseCtx<string> ctx) mutable {
        this->sock.on("end", [=]() mutable {
            ctx.resolve(this->recv_data);
            //Close data socket
            this->sock.close();
        });
    });
    //Connect to given address and port
    this->sock.connect(this->addr, this->port);
    
    return promise;
}
