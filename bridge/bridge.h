#pragma once

#ifdef __cplusplus
extern "C" {
#endif

//Initialize promise module (Bridge method)
void promise_init();
//Initialize reactor module (Bridge method)
void reactor_init();
//Run task loop once
void run_task_loop_once();

#ifdef __cplusplus
}
#endif
